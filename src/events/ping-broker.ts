import { TPing } from "../models/TPing";

export type PingBroker = {
  emitSavePing: (ping: TPing) => Promise<void>;
};
