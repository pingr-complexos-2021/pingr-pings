import MongoDB from "mongodb";

const connectionURL = process.env["MONGO_URL"];

export const MongoFactory = () => {
  const client = new MongoDB.MongoClient(connectionURL || "", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  return client;
};
