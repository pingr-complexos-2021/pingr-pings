import stan from "node-nats-streaming";

export const NatsFactory = () => {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env["BROKER_URL"] || "",
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");
  });

  return conn;
};
