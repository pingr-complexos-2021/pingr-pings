export class InvalidPingError extends Error {
  name = "InvalidPingError";

  constructor(public reason: string) {
    super();
  }
}

export enum InvalidPingErrorReasons {
  TEXT_TOO_LONG = "TEXT_TOO_LONG",
  EMPTY_BODY_AND_MEDIA = "EMPTY_BODY_AND_MEDIA",
  TOO_MUCH_TAGS = "TOO_MUCH_TAGS",
  INVALID_TAG = "INVALID_TAG",
}
