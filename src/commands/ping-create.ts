import { v4 as uuid } from "uuid";
import { PingRepository } from "../data/ping-repository";
import { PingBroker } from "../events/ping-broker";
import { PingVisibility, TPing } from "../models/TPing";
import {
  InvalidPingError,
  InvalidPingErrorReasons,
} from "./errors/InvalidPingTextError";

export type PingCreateForm = {
  userId: string;
  text: string;
  tags: string[];
  imageURLs: string[];
  visibilityOverride?: PingVisibility;
};

export type PingCreateCommand = (form: PingCreateForm) => Promise<void>;

export const PingCreateCommand = ({
  repository,
  broker,
}: {
  repository: PingRepository;
  broker: PingBroker;
}) => async (form: PingCreateForm): Promise<void> => {
  if (form.text.length > 140)
    throw new InvalidPingError(InvalidPingErrorReasons.TEXT_TOO_LONG);

  if (form.imageURLs.length === 0 && form.text.length === 0)
    throw new InvalidPingError(InvalidPingErrorReasons.EMPTY_BODY_AND_MEDIA);

  if (form.tags.length > 10)
    throw new InvalidPingError(InvalidPingErrorReasons.TOO_MUCH_TAGS);

  form.tags.forEach((tag) => {
    if (tag.includes(" "))
      throw new InvalidPingError(InvalidPingErrorReasons.INVALID_TAG);
    if (tag === "")
      throw new InvalidPingError(InvalidPingErrorReasons.INVALID_TAG);
  });

  if (form.tags.reduce((acc, curr) => acc + curr.length, 0) > 140)
    throw new InvalidPingError(InvalidPingErrorReasons.TOO_MUCH_TAGS);

  const ping: TPing = {
    userId: form.userId,
    date: new Date(),
    imageURLs: form.imageURLs,
    tags: form.tags,
    text: form.text,
    visibilityOverride: form.visibilityOverride,
    id: uuid(),
  };

  await repository.savePing(ping);
  await broker.emitSavePing(ping);
};
