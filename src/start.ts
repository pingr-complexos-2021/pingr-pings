import { CorsOptions } from "cors";
import { ApiFactory as API } from "./api";

const PORT = process.env["API_PORT"];
const ALLOWED_ORIGINS = JSON.parse(process.env["API_ALLOWED_ORIGINS"] || "");

const corsOptions: CorsOptions = {
  origin: function (origin, callback) {
    if (ALLOWED_ORIGINS.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};

async function main() {
  try {
    const api = API({ corsOptions });
    api.listen(PORT, () =>
      console.log(`Listening at http://localhost:${PORT}`)
    );
  } catch (e) {
    console.dir(e);
  }
}

main();
