export enum PingVisibility {
  public,
  private,
  restricted,
}

export type TPing = {
  id: string;
  userId: string;
  text: string;
  tags: string[];
  imageURLs: string[];
  visibilityOverride?: PingVisibility;
  date: Date;
};
