export enum UserVisibility {
  public,
  private,
}

export type TUser = {
  id: string;
  visibility: UserVisibility;
  followers?: TUser[];
  specialFriends?: TUser[];
};
