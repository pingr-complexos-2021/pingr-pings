import { TPing } from "../models/TPing";

export type PingRepository = {
  savePing: (ping: TPing) => Promise<void>;
};
