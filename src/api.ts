import express, { json } from "express";
import cors, { CorsOptions } from "cors";

export const ApiFactory = ({ corsOptions }: { corsOptions: CorsOptions }) => {
  const api = express();
  api.use(json());
  api.use(cors(corsOptions));

  api.get("/", (_, res) => res.json("Hello, Ekans!"));

  return api;
};
