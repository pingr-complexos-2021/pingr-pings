module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:jest/recommended",
    "prettier",
    "plugin:node/recommended",
  ],
  parserOptions: {
    ecmaVersion: 2021,
  },
  plugins: ["jest", "prettier"],
  rules: {
    "prettier/prettier": "error",
    "no-unused-vars": "warn",
    "no-console": "off",
    "func-names": "off",
    "no-process-exit": "off",
    "object-shorthand": "off",
    "class-methods-use-this": "off",
    "node/no-unpublished-require": "off",
  },
};
