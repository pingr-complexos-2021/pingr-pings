// External libraries
import { Express } from "express-serve-static-core";
import request from "supertest";

// API module
import { ApiFactory as API } from "../src/api";

describe("The project", () => {
  let api: Express;
  const corsOptions = { origin: "*" };

  beforeEach(() => {
    api = API({ corsOptions });
  });

  it("can use Jest", () => {
    expect(true).toBe(true);
  });

  it("can use Supertest", async () => {
    const response = await request(api).get("/");
    expect(response.status).toBe(200);
    expect(response.body).toBe("Hello, Ekans!");
  });

  it("can use CORS", async () => {
    const response = await request(api).get("/");
    const cors_header = response.header["access-control-allow-origin"];
    expect(cors_header).toBe("*");
  });
});
