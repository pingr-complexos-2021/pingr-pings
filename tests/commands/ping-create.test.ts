import faker from "faker";
import {
  InvalidPingError,
  InvalidPingErrorReasons,
} from "../../src/commands/errors/InvalidPingTextError";
import { PingCreateCommand } from "../../src/commands/ping-create";

describe("Create ping", () => {
  let pingCreate: PingCreateCommand;
  let repository = { savePing: jest.fn() };
  let broker = { emitSavePing: jest.fn() };

  beforeEach(() => {
    repository.savePing.mockClear();
    broker.emitSavePing.mockClear();

    pingCreate = PingCreateCommand({
      repository,
      broker,
    });
  });

  test("Err on > 140 characters", async () => {
    const text = faker.random.alphaNumeric(141);

    const err = await pingCreate({
      text,
      imageURLs: [],
      tags: [],
      userId: faker.random.alphaNumeric(50),
    }).catch((err) => err);

    expect(err).toBeInstanceOf(Error);
    expect(err.name).toStrictEqual(InvalidPingError.name);
    expect(err.reason).toEqual(InvalidPingErrorReasons.TEXT_TOO_LONG);
  });

  test("Err on empty body and no media", async () => {
    const text = "";
    const imageURLs: string[] = [];

    const err = await pingCreate({
      text,
      imageURLs,
      tags: [],
      userId: faker.random.alphaNumeric(50),
    }).catch((err) => err);

    expect(err).toBeInstanceOf(Error);
    expect(err.name).toStrictEqual(InvalidPingError.name);
    expect(err.reason).toEqual(InvalidPingErrorReasons.EMPTY_BODY_AND_MEDIA);
  });

  test("Err on > 10 hashtags", async () => {
    const text = faker.random.alphaNumeric(120);
    const tags = Array(11).map(() => faker.random.alphaNumeric(5));

    const err = await pingCreate({
      text,
      tags,
      imageURLs: [],
      userId: faker.random.alphaNumeric(50),
    }).catch((err) => err);

    expect(err).toBeInstanceOf(Error);
    expect(err.name).toStrictEqual(InvalidPingError.name);
    expect(err.reason).toEqual(InvalidPingErrorReasons.TOO_MUCH_TAGS);
  });

  test("Prevent empty hashtag", async () => {
    const text = faker.random.alphaNumeric(120);
    const tags = [""];

    const err = await pingCreate({
      text,
      tags,
      imageURLs: [],
      userId: faker.random.alphaNumeric(50),
    }).catch((err) => err);

    expect(err).toBeInstanceOf(Error);
    expect(err.name).toStrictEqual(InvalidPingError.name);
    expect(err.reason).toEqual(InvalidPingErrorReasons.INVALID_TAG);
  });

  test("Prevent hashtag with spaces", async () => {
    const text = faker.random.alphaNumeric(120);
    const tags = ["P2 "];

    const err = await pingCreate({
      text,
      tags,
      imageURLs: [],
      userId: faker.random.alphaNumeric(50),
    }).catch((err) => err);

    expect(err).toBeInstanceOf(Error);
    expect(err.name).toStrictEqual(InvalidPingError.name);
    expect(err.reason).toEqual(InvalidPingErrorReasons.INVALID_TAG);
  });

  test("Prevent hashtags that exceed 140 characters", async () => {
    const text = faker.random.alphaNumeric(120);
    const tags = Array(150).map(() => faker.random.alphaNumeric(15));

    const err = await pingCreate({
      text,
      tags,
      imageURLs: [],
      userId: faker.random.alphaNumeric(50),
    }).catch((err) => err);

    expect(err).toBeInstanceOf(Error);
    expect(err.name).toStrictEqual(InvalidPingError.name);
    expect(err.reason).toEqual(InvalidPingErrorReasons.TOO_MUCH_TAGS);
  });

  test("Save a ping", async () => {
    const text = faker.random.alphaNumeric(120);
    const tags = Array(10).map(() => faker.random.alphaNumeric(8));

    await pingCreate({
      text,
      tags,
      imageURLs: [],
      userId: faker.random.alphaNumeric(50),
    });

    expect(repository.savePing).toHaveBeenCalled();
    expect(broker.emitSavePing).toHaveBeenCalled();
  });
});
